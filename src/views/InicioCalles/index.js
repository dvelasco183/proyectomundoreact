import { useState, useEffect } from "react";
import EncabezadoInicio from "./components/EncabezadoInicio";
import Combobox from "./components/Combobox";
import ResultsCombobox from "./components/ResultsCombobox";
import "./style.css";

export default function InicioCalles() {
    const [datosRegion, setDatosRegion] = useState([]);
    const [idRegion, setIdRegion] = useState();
    useEffect(async () => {
        try {
            const response = await fetch("http://127.0.0.1:8000/api/DatosRegion");
            const datosRegion = await response.json();
            setDatosRegion(datosRegion);
        } catch (err) {
            console.error(err);
        }
    }, []);

    const [idProvincia, setIdProvincia] = useState();
    const [DatosProvincias, setDatosProvincias] = useState([]);
    useEffect(async () => {
        try {
            const response = await fetch("http://127.0.0.1:8000/api/DatosProvincia", {
                method: 'post',
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    idRegion: idRegion,
                })
            });
            const DatosProvincias = await response.json();
            setDatosProvincias(DatosProvincias);
        } catch (err) {
            console.error(err);
        }
    }, [idRegion]);

    const [idCiudad, setIdCiudad] = useState();
    const [DatosCiudades, setDatosCiudades] = useState([]);
    useEffect(async () => {
        try {
            const response = await fetch("http://127.0.0.1:8000/api/DatosCiudad", {
                method: 'post',
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    idProvincia: idProvincia,
                })
            });
            const DatosCiudades = await response.json();
            setDatosCiudades(DatosCiudades);
        } catch (err) {
            console.error(err);
        }
    }, [idProvincia]);

    const [idCalle, setIdCalle] = useState();
    const [DatosCalle, setDatosCalle] = useState([]);

    useEffect(async () => {
        try {
            const response = await fetch("http://127.0.0.1:8000/api/DatosCalle", {
                method: 'post',
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    idCiudad: idCiudad,
                })
            });
            const DatosCalle = await response.json();
            setDatosCalle(DatosCalle);
        } catch (err) {
            console.error(err);
        }
    }, [idCiudad]);

    return (
        <div className="CalleInicio" >
            <EncabezadoInicio />
            <div className="div-combobox">
                <Combobox titulo={"Región"} datos={datosRegion} onClick={({ target: { value } }) => setIdRegion(value)} />
                <Combobox titulo={"Provincia"} datos={DatosProvincias} onClick={({ target: { value } }) => setIdProvincia(value)} deshabilitado={idRegion ? false : true} />
                <Combobox titulo={"Ciudad"} datos={DatosCiudades} onClick={({ target: { value } }) => setIdCiudad(value)} deshabilitado={idProvincia ? false : true} />
            </div>
            <ResultsCombobox datos={DatosCalle} />
        </div>
    );
}
