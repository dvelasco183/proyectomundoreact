import 'bootstrap/dist/css/bootstrap.min.css';
import { useState, useEffect } from 'react';
import { Button } from 'reactstrap';
import Combobox from "../Combobox";
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

export default function NombreCalle(props) {
    const [nombreCalle, setnombreCalle] = useState("");
    const handleNombreCalleChange = ({ target: { value } }) => setnombreCalle(value);

    const [datosRegion, setDatosRegion] = useState([]);
    const [idRegion, setIdRegion] = useState();
    useEffect(async () => {
        try {
            const response = await fetch("http://127.0.0.1:8000/api/DatosRegion");
            const datosRegion = await response.json();
            setDatosRegion(datosRegion);
        } catch (err) {
            console.error(err);
        }
    }, []);

    const [idProvincia, setIdProvincia] = useState();
    const [DatosProvincias, setDatosProvincias] = useState([]);
    useEffect(async () => {
        try {
            const response = await fetch("http://127.0.0.1:8000/api/DatosProvincia", {
                method: 'post',
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    idRegion: idRegion,
                })
            });
            const DatosProvincias = await response.json();
            setDatosProvincias(DatosProvincias);
        } catch (err) {
            console.error(err);
        }
    }, [idRegion]);

    const [idCiudad, setIdCiudad] = useState();
    const [DatosCiudades, setDatosCiudades] = useState([]);
    useEffect(async () => {
        try {
            const response = await fetch("http://127.0.0.1:8000/api/DatosCiudad", {
                method: 'post',
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    idProvincia: idProvincia,
                })
            });
            const DatosCiudades = await response.json();
            setDatosCiudades(DatosCiudades);
        } catch (err) {
            console.error(err);
        }
    }, [idProvincia]);

    const [DatosRespuesta, setDatosRespuesta] = useState([]);


    const enviarDatos = async (event) => {
        event.preventDefault();

        try {
            const response = await fetch("http://127.0.0.1:8000/api/InsertCalles", {
                method: 'post',
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    nombreCalle: nombreCalle,
                    idCiudad: idCiudad,
                })
            });
            const DatosRespuesta = await response.json();
            setDatosRespuesta(DatosRespuesta);
            SwalRealizado();
        } catch (err) {
            console.error(err);
        }
    }
    
     const SwalRealizado = () => {
        const MySwal = withReactContent(Swal);
        MySwal.fire({
            icon: 'success',
            title: 'Registrado',
            text: 'La calle se ha registrado correctamente.',
          confirmButtonText:'Confirmar',
          }).then(()=>{
              props.cerrar();
          })
    }

    return (
        <div>
            <form onSubmit={enviarDatos}>
                <Combobox titulo={"Región"} datos={datosRegion} onClick={({ target: { value } }) => setIdRegion(value)} />
                <Combobox titulo={"Provincia"} datos={DatosProvincias} onClick={({ target: { value } }) => setIdProvincia(value)} deshabilitado={idRegion ? false : true} />
                <Combobox titulo={"Ciudad"} datos={DatosCiudades} onClick={({ target: { value } }) => setIdCiudad(value)} deshabilitado={idProvincia ? false : true} />
                Ingrese el nombre de la calle:
                <input type="text" value={nombreCalle} onChange={handleNombreCalleChange} className="form-control" />
                <br></br>
                <Button color="success" type="submit" value="Submit" disabled={nombreCalle && idRegion && idProvincia && idCiudad ? false : true} > Crear Calle</Button>
            </form>
        </div>
    );
}