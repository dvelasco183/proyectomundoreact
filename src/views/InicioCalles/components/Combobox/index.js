import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Dropdown, DropdownItem, DropdownMenu, DropdownToggle } from 'reactstrap';
import './style.css';

export default function Combobox(props) {
    const [dropdown, setDropdown] = useState(false);
    const abrirCerrarDropdown = () => {
        setDropdown(!dropdown);
    }
    return (
        <div className="Combobox" >
            <b>{props.titulo}</b>
            <Dropdown isOpen={dropdown} toggle={abrirCerrarDropdown}>
                <DropdownToggle caret disabled={props.deshabilitado}>
                    Seleccione un opción
                </DropdownToggle>
                <DropdownMenu>
                    {props.datos.map((item, index) => {
                        return <DropdownItem value={item.id} key={index} onClick={props.onClick}> {item.nombre}</DropdownItem>
                    })}
                </DropdownMenu>
            </Dropdown>
        </div>
    );
}