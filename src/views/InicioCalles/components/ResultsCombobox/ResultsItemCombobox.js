import React from 'react';
import { useState } from "react";
import { Button, Table, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

export default function ResultsItemCombobox(props) {

    const [nombreCalle, setnombreCalle] = useState("");
    const [idCalle, setIdCalle] = useState("");
    const handleNombreCalleChange = ({ target: { value } }) => setnombreCalle(value);
    const [modal, setModal] = useState(false);

    const toggle = (NombreCalle, idCalle) => {
        if (NombreCalle != undefined) { setnombreCalle(NombreCalle); setIdCalle(idCalle); }
        setModal(!modal)
    };

    const [DatosRespuesta, setDatosRespuesta] = useState([]);
    const enviarDatos = async (event) => {
        event.preventDefault();
        try {
            const response = await fetch("http://127.0.0.1:8000/api/ModificarCalles", {
                method: 'put',
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    nombreCalle: nombreCalle,
                    idCalle: idCalle,
                })
            });
            const DatosRespuesta = await response.json();
            setDatosRespuesta(DatosRespuesta);
            SwalRealizado();
        } catch (err) {
            console.error(err);
        }
    }

    const SwalRealizado = () => {
        const MySwal = withReactContent(Swal);
        MySwal.fire({
            icon: 'success',
            title: 'Actualizado',
            text: 'La calle se ha Actualizado correctamente.',
            confirmButtonText: 'Confirmar',
        }).then(() => {
            setModal(!modal);
        })
    }

    return (
        <div style={{
            marginTop: "1rem",
            marginBottom: "1rem",
            padding: 10,
            width: "100%",
        }}>
            <Table bordered hover responsive>
                <tbody>
                    {props.datos.map((item, index) => {
                        return <tr>
                            <th scope="row">
                                {item.nombre}
                            </th>
                            <td>
                                <Button color="primary" onClick={() => toggle(item.nombre, item.id)}>Editar Calle</Button>

                                <Modal isOpen={modal} toggle={toggle}>
                                    <ModalHeader>Actualizar Calle.</ModalHeader>
                                    <ModalBody>
                                        <form onSubmit={enviarDatos}>
                                            <p>Nombre Calle:</p>
                                            <input type="text" value={nombreCalle} onChange={handleNombreCalleChange} className="form-control" />
                                            <br></br>
                                            <Button color="success" type="submit" value="Submit" disabled={nombreCalle ? false : true} > Crear Calle</Button>
                                        </form>
                                    </ModalBody>
                                    <ModalFooter>
                                        <Button color="primary" onClick={toggle}>
                                            Cerrar{' '}
                                        </Button>
                                    </ModalFooter>
                                </Modal>
                            </td>
                        </tr>
                    })}
                </tbody>
            </Table>
        </div>
    );
}