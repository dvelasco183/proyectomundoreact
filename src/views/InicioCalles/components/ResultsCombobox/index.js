import ResultsItemCombobox from "./ResultsItemCombobox";
import { Table } from 'reactstrap';

export default function ResultCombobox(props) {
    return (
        <div style={{
            width: "60%",
            padding: "2rem 2rem 0rem 2rem",
            textAlign: 'center',
        }}>
            <Table>
                <thead>
                    <tr>
                        <th>
                            Calles Asociadas
                        </th>
                    </tr>
                </thead>
            </Table>
            <ResultsItemCombobox datos={props.datos} />
        </div>
    );
}

