import { useState } from "react";
import NombreCalle from "../NombreCalle";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import "./style.css";

export default function EncabezadoInicio() {
    const [modal, setModal] = useState(false);
    const toggle = () => setModal(!modal);

    return (
        <div className="EncabezadoInicio" >
            <div className="DivMantenedor">
                <h1><b> Mantenedor De Calles</b></h1>
            </div>
            <div className="DivButton">
                <Button color="success" onClick={toggle}> Crear Calle</Button>
            </div>
            <Modal isOpen={modal} toggle={toggle}>
                <ModalHeader>Crear Calle</ModalHeader>
                <ModalBody>
                    <NombreCalle cerrar={toggle} />
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={toggle}>
                        Cerrar{' '}
                    </Button>
                </ModalFooter>
            </Modal>

        </div>
    );


}

